*** Settings ***

Library         Selenium2Library

*** Variables ***
${URL}          https://www.airasia.com/th/th/home.page

*** Test Cases ***
Locate Search Button and Click It

    open browser         ${URL}         chrome
    maximize browser window
    set selenium speed  1s
    click element       //*[@id="header"]/div/div[8]/span
#     click element       //div/span[normalize-space(text())='ล็อกอิน/สมัครสมาชิก']
#     //*[@id="username-input--login"]
#//div/div/div/form/div/div/div/input[@type='text']/../label[@for='username-input--login' and text()='อีเมล']
    input text          id:username-input--login            Beer@gmail.com


    input text          id:password-input--login            123456
    click element       id:loginbutton
    element should contain   //*[@id="aaw-error"]        รหัสผ่านต้องมีอักขระทั้งตัวเลขและตัวหนังสือรวมกันจำนวน 8-15 ตัว พร้อมข้อกำหนดต่อไปนี้ ตัวเลข (0-9), อักษรภาษาอังกฤษตัวพิมพ์เล็ก (a-z), อักษรภาษาอังกฤษตัวพิมพ์ใหญ่ (A-Z). หากรหัสผ่านของท่านไม่ถูกต้องตามข้อกำหนด กรุณา


#Test Teardown
    #close all browsers

*** Keywords ***
